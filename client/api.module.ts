import * as api from './api/api';
import * as angular from 'angular';

const apiModule = angular.module('api', [])
.service('CommentsApi', api.CommentsApi)
.service('CommitteesApi', api.CommitteesApi)
.service('DocumentsApi', api.DocumentsApi)
.service('DomainsApi', api.DomainsApi)
.service('MeetingsApi', api.MeetingsApi)
.service('TasksApi', api.TasksApi)
.service('UsersApi', api.UsersApi)
.service('VersionsApi', api.VersionsApi)

export default apiModule;
