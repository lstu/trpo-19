/**
 * Minutes API
 * Business logic API for Minutes system
 *
 * OpenAPI spec version: 1.9
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from '../model/models';

/* tslint:disable:no-unused-variable member-ordering */

export class CommentsApi {
    protected basePath = 'https://10.178.7.178/minutes-api';
    public defaultHeaders : any = {};

    static $inject: string[] = ['$http', '$httpParamSerializer', 'basePath'];

    constructor(protected $http: ng.IHttpService, protected $httpParamSerializer?: (d: any) => any, basePath?: string) {
        if (basePath !== undefined) {
            this.basePath = basePath;
        }
    }

    /**
     * Cycles are not allowed. Comment date is ignored and taken at the server-side.
     * @summary ALL: add a new comment or a subcomment to the task [personal-right]
     * @param comment 
     */
    public addComment (comment?: models.CommentCard, extraHttpRequestParams?: any ) : ng.IHttpPromise<models.CommentCard> {
        const localVarPath = this.basePath + '/comments';

        let queryParameters: any = {};
        let headerParams: any = (<any>Object).assign({}, this.defaultHeaders);
        let httpRequestParams: ng.IRequestConfig = {
            method: 'POST',
            url: localVarPath,
            data: comment,
            params: queryParameters,
            headers: headerParams
        };

        if (extraHttpRequestParams) {
            httpRequestParams = (<any>Object).assign(httpRequestParams, extraHttpRequestParams);
        }

        return this.$http(httpRequestParams);
    }
    /**
     * 
     * @summary CA: change the comment [personal-right]
     * @param commId 
     * @param commText 
     */
    public changeComment (commId: number, commText: models.Text, extraHttpRequestParams?: any ) : ng.IHttpPromise<models.Text> {
        const localVarPath = this.basePath + '/comments/{comm_id}'
            .replace('{' + 'comm_id' + '}', encodeURIComponent(String(commId)));

        let queryParameters: any = {};
        let headerParams: any = (<any>Object).assign({}, this.defaultHeaders);
        // verify required parameter 'commId' is not null or undefined
        if (commId === null || commId === undefined) {
            throw new Error('Required parameter commId was null or undefined when calling changeComment.');
        }

        // verify required parameter 'commText' is not null or undefined
        if (commText === null || commText === undefined) {
            throw new Error('Required parameter commText was null or undefined when calling changeComment.');
        }

        let httpRequestParams: ng.IRequestConfig = {
            method: 'POST',
            url: localVarPath,
            data: commText,
            params: queryParameters,
            headers: headerParams
        };

        if (extraHttpRequestParams) {
            httpRequestParams = (<any>Object).assign(httpRequestParams, extraHttpRequestParams);
        }

        return this.$http(httpRequestParams);
    }
    /**
     * 
     * @summary ALL: get last N comments [system-right]
     * @param n 
     */
    public getComments (n?: number, extraHttpRequestParams?: any ) : ng.IHttpPromise<Array<models.CommentCard>> {
        const localVarPath = this.basePath + '/comments';

        let queryParameters: any = {};
        let headerParams: any = (<any>Object).assign({}, this.defaultHeaders);
        if (n !== undefined) {
            queryParameters['n'] = n;
        }

        let httpRequestParams: ng.IRequestConfig = {
            method: 'GET',
            url: localVarPath,
            params: queryParameters,
            headers: headerParams
        };

        if (extraHttpRequestParams) {
            httpRequestParams = (<any>Object).assign(httpRequestParams, extraHttpRequestParams);
        }

        return this.$http(httpRequestParams);
    }
    /**
     * 
     * @summary CA (Comment Author): remove the comment [personal-right]
     * @param commId 
     */
    public removeComment (commId: number, extraHttpRequestParams?: any ) : ng.IHttpPromise<{}> {
        const localVarPath = this.basePath + '/comments/{comm_id}'
            .replace('{' + 'comm_id' + '}', encodeURIComponent(String(commId)));

        let queryParameters: any = {};
        let headerParams: any = (<any>Object).assign({}, this.defaultHeaders);
        // verify required parameter 'commId' is not null or undefined
        if (commId === null || commId === undefined) {
            throw new Error('Required parameter commId was null or undefined when calling removeComment.');
        }

        let httpRequestParams: ng.IRequestConfig = {
            method: 'DELETE',
            url: localVarPath,
            params: queryParameters,
            headers: headerParams
        };

        if (extraHttpRequestParams) {
            httpRequestParams = (<any>Object).assign(httpRequestParams, extraHttpRequestParams);
        }

        return this.$http(httpRequestParams);
    }
    /**
     * Contatenates the comment with the current contents of report field for all languages. Shown as an icon in the task line.
     * @summary CM: add the comment to the task's report [committee-main-task]
     * @param commId 
     */
    public reportComment (commId: number, extraHttpRequestParams?: any ) : ng.IHttpPromise<{}> {
        const localVarPath = this.basePath + '/comments/{comm_id}/report'
            .replace('{' + 'comm_id' + '}', encodeURIComponent(String(commId)));

        let queryParameters: any = {};
        let headerParams: any = (<any>Object).assign({}, this.defaultHeaders);
        // verify required parameter 'commId' is not null or undefined
        if (commId === null || commId === undefined) {
            throw new Error('Required parameter commId was null or undefined when calling reportComment.');
        }

        let httpRequestParams: ng.IRequestConfig = {
            method: 'POST',
            url: localVarPath,
            params: queryParameters,
            headers: headerParams
        };

        if (extraHttpRequestParams) {
            httpRequestParams = (<any>Object).assign(httpRequestParams, extraHttpRequestParams);
        }

        return this.$http(httpRequestParams);
    }
}
