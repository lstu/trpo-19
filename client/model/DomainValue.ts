/**
 * Minutes API
 * Business logic API for Minutes system
 *
 * OpenAPI spec version: 1.9
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface DomainValue {
    "valId"?: number;
    "domId"?: number;
    "valStatus"?: DomainValue.ValStatusEnum;
    "valCode"?: string;
    "valAbbr"?: string;
    "valName"?: string;
    "metValId"?: number;
    "valElType"?: number;
    "props"?: string;
}

export namespace DomainValue {
    export enum ValStatusEnum {
        N = <any> 'N',
        A = <any> 'A',
        C = <any> 'C',
        R = <any> 'R'
    }
}
