package io.swagger.api;

import io.swagger.model.CommitteeCard;
import io.swagger.model.MeetingCard;
import io.swagger.model.TaskCard;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

@Controller
public class CommitteesApiController implements CommitteesApi {

    private static final Logger log = LoggerFactory.getLogger(CommitteesApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public CommitteesApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<CommitteeCard> addCommittee(@ApiParam(value = ""  )  @Valid @RequestBody CommitteeCard cmt) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<CommitteeCard>(objectMapper.readValue("{  \"cmt_announcement\" : \"\",  \"cmt_id\" : 1,  \"cmt_code\" : \"QUALITY\",  \"cmt_location\" : \"cmt_location\",  \"cmt_status\" : 1,  \"cmt_next_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"cmt_topic\" : \"\",  \"cmt_pattern\" : \"cmt_pattern\",  \"cmt_name\" : \"\",  \"attendances\" : [ {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  }, {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  } ]}", CommitteeCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<CommitteeCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<CommitteeCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<CommitteeCard> changeCommittee(@ApiParam(value = "",required=true) @PathVariable("cmt_id") Integer cmtId,@ApiParam(value = ""  )  @Valid @RequestBody CommitteeCard cmt) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<CommitteeCard>(objectMapper.readValue("{  \"cmt_announcement\" : \"\",  \"cmt_id\" : 1,  \"cmt_code\" : \"QUALITY\",  \"cmt_location\" : \"cmt_location\",  \"cmt_status\" : 1,  \"cmt_next_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"cmt_topic\" : \"\",  \"cmt_pattern\" : \"cmt_pattern\",  \"cmt_name\" : \"\",  \"attendances\" : [ {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  }, {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  } ]}", CommitteeCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<CommitteeCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<CommitteeCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<CommitteeCard>> getCommittee(@ApiParam(value = "",required=true) @PathVariable("cmt_id") Integer cmtId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<CommitteeCard>>(objectMapper.readValue("[ {  \"cmt_announcement\" : \"\",  \"cmt_id\" : 1,  \"cmt_code\" : \"QUALITY\",  \"cmt_location\" : \"cmt_location\",  \"cmt_status\" : 1,  \"cmt_next_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"cmt_topic\" : \"\",  \"cmt_pattern\" : \"cmt_pattern\",  \"cmt_name\" : \"\",  \"attendances\" : [ {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  }, {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  } ]}, {  \"cmt_announcement\" : \"\",  \"cmt_id\" : 1,  \"cmt_code\" : \"QUALITY\",  \"cmt_location\" : \"cmt_location\",  \"cmt_status\" : 1,  \"cmt_next_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"cmt_topic\" : \"\",  \"cmt_pattern\" : \"cmt_pattern\",  \"cmt_name\" : \"\",  \"attendances\" : [ {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  }, {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  } ]} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<CommitteeCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<CommitteeCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<TaskCard>> getCommitteeTasks(@ApiParam(value = "",required=true) @PathVariable("cmt_id") Integer cmtId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<TaskCard>>(objectMapper.readValue("[ {  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}, {  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<TaskCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<TaskCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<CommitteeCard>> getCommittees(@ApiParam(value = "status code, not id") @Valid @RequestParam(value = "committee_status", required = false) String committeeStatus,@ApiParam(value = "") @Valid @RequestParam(value = "new_para", required = false) String newPara) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<CommitteeCard>>(objectMapper.readValue("[ {  \"cmt_announcement\" : \"\",  \"cmt_id\" : 1,  \"cmt_code\" : \"QUALITY\",  \"cmt_location\" : \"cmt_location\",  \"cmt_status\" : 1,  \"cmt_next_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"cmt_topic\" : \"\",  \"cmt_pattern\" : \"cmt_pattern\",  \"cmt_name\" : \"\",  \"attendances\" : [ {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  }, {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  } ]}, {  \"cmt_announcement\" : \"\",  \"cmt_id\" : 1,  \"cmt_code\" : \"QUALITY\",  \"cmt_location\" : \"cmt_location\",  \"cmt_status\" : 1,  \"cmt_next_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"cmt_topic\" : \"\",  \"cmt_pattern\" : \"cmt_pattern\",  \"cmt_name\" : \"\",  \"attendances\" : [ {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  }, {    \"user_id\" : 0,    \"member_role\" : 6,    \"email_notification\" : true  } ]} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<CommitteeCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<CommitteeCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<MeetingCard>> getMeetings(@ApiParam(value = "",required=true) @PathVariable("cmt_id") Integer cmtId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<MeetingCard>>(objectMapper.readValue("[ {  \"meeting_status\" : 1,  \"presenses\" : [ {    \"pres_type\" : 5,    \"user_id\" : 1  }, {    \"pres_type\" : 5,    \"user_id\" : 1  } ],  \"cmt_id\" : 0,  \"meeting_id\" : 1,  \"moderator_id\" : 6,  \"meeting_notes\" : \"\",  \"meeting_fin_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"meeting_start_dt\" : \"2000-01-23T04:56:07.000+00:00\"}, {  \"meeting_status\" : 1,  \"presenses\" : [ {    \"pres_type\" : 5,    \"user_id\" : 1  }, {    \"pres_type\" : 5,    \"user_id\" : 1  } ],  \"cmt_id\" : 0,  \"meeting_id\" : 1,  \"moderator_id\" : 6,  \"meeting_notes\" : \"\",  \"meeting_fin_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"meeting_start_dt\" : \"2000-01-23T04:56:07.000+00:00\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<MeetingCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<MeetingCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<TaskCard> proposeTask(@ApiParam(value = "",required=true) @PathVariable("cmt_id") Integer cmtId,@ApiParam(value = "" ,required=true )  @Valid @RequestBody TaskCard task) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TaskCard>(objectMapper.readValue("{  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}", TaskCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TaskCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TaskCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> removeCommittee(@ApiParam(value = "",required=true) @PathVariable("cmt_id") Integer cmtId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
