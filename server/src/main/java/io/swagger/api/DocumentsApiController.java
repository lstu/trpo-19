package io.swagger.api;

import io.swagger.model.DocumentCard;
import io.swagger.model.VersionCard;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

@Controller
public class DocumentsApiController implements DocumentsApi {

    private static final Logger log = LoggerFactory.getLogger(DocumentsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public DocumentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<VersionCard> addFileVersion(@ApiParam(value = "",required=true) @PathVariable("doc_id") Integer docId,@ApiParam(value = ""  )  @Valid @RequestBody VersionCard version) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<VersionCard>(objectMapper.readValue("{  \"docver_status\" : 5,  \"docver_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"user_id\" : 6,  \"docver_notes\" : \"\",  \"file_id\" : 5,  \"docver_fname\" : \"docver_fname\",  \"docver_id\" : 0,  \"doc_id\" : 1,  \"docver_num\" : \"docver_num\"}", VersionCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<VersionCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<VersionCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<DocumentCard> changeDocument(@ApiParam(value = "",required=true) @PathVariable("doc_id") Integer docId,@ApiParam(value = ""  )  @Valid @RequestBody DocumentCard document) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<DocumentCard>(objectMapper.readValue("{  \"doc_stat\" : 1,  \"doc_source_doc\" : 5,  \"doc_name\" : \"\",  \"task_id\" : 0,  \"doc_type\" : 6,  \"doc_id\" : 1,  \"doc_notes\" : \"\"}", DocumentCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<DocumentCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<DocumentCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<VersionCard>> getDocumentVersions(@ApiParam(value = "",required=true) @PathVariable("doc_id") Integer docId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<VersionCard>>(objectMapper.readValue("[ {  \"docver_status\" : 5,  \"docver_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"user_id\" : 6,  \"docver_notes\" : \"\",  \"file_id\" : 5,  \"docver_fname\" : \"docver_fname\",  \"docver_id\" : 0,  \"doc_id\" : 1,  \"docver_num\" : \"docver_num\"}, {  \"docver_status\" : 5,  \"docver_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"user_id\" : 6,  \"docver_notes\" : \"\",  \"file_id\" : 5,  \"docver_fname\" : \"docver_fname\",  \"docver_id\" : 0,  \"doc_id\" : 1,  \"docver_num\" : \"docver_num\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<VersionCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<VersionCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> getFileDocument(@ApiParam(value = "",required=true) @PathVariable("doc_id") Integer docId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> removeDocument(@ApiParam(value = "",required=true) @PathVariable("doc_id") Integer docId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
