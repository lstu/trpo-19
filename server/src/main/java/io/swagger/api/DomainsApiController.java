package io.swagger.api;

import io.swagger.model.Domain;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

@Controller
public class DomainsApiController implements DomainsApi {

    private static final Logger log = LoggerFactory.getLogger(DomainsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public DomainsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<List<Domain>> getDomains() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Domain>>(objectMapper.readValue("[ {  \"uom_id\" : 6,  \"el_type_dom\" : 5,  \"values\" : [ {    \"val_code\" : \"val_code\",    \"val_status\" : \"N\",    \"val_abbr\" : \"val_abbr\",    \"val_name\" : \"val_name\",    \"val_el_type\" : 9,    \"dom_id\" : 2,    \"val_id\" : 5,    \"met_val_id\" : 7,    \"props\" : \"props\"  }, {    \"val_code\" : \"val_code\",    \"val_status\" : \"N\",    \"val_abbr\" : \"val_abbr\",    \"val_name\" : \"val_name\",    \"val_el_type\" : 9,    \"dom_id\" : 2,    \"val_id\" : 5,    \"met_val_id\" : 7,    \"props\" : \"props\"  } ],  \"dom_code\" : \"dom_code\",  \"dom_id\" : 0,  \"dom_type\" : 1,  \"dom_abbr\" : \"dom_abbr\",  \"dom_name\" : \"dom_name\",  \"props\" : \"props\"}, {  \"uom_id\" : 6,  \"el_type_dom\" : 5,  \"values\" : [ {    \"val_code\" : \"val_code\",    \"val_status\" : \"N\",    \"val_abbr\" : \"val_abbr\",    \"val_name\" : \"val_name\",    \"val_el_type\" : 9,    \"dom_id\" : 2,    \"val_id\" : 5,    \"met_val_id\" : 7,    \"props\" : \"props\"  }, {    \"val_code\" : \"val_code\",    \"val_status\" : \"N\",    \"val_abbr\" : \"val_abbr\",    \"val_name\" : \"val_name\",    \"val_el_type\" : 9,    \"dom_id\" : 2,    \"val_id\" : 5,    \"met_val_id\" : 7,    \"props\" : \"props\"  } ],  \"dom_code\" : \"dom_code\",  \"dom_id\" : 0,  \"dom_type\" : 1,  \"dom_abbr\" : \"dom_abbr\",  \"dom_name\" : \"dom_name\",  \"props\" : \"props\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Domain>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Domain>>(HttpStatus.NOT_IMPLEMENTED);
    }

}
