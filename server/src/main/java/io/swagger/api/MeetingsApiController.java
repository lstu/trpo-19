package io.swagger.api;

import io.swagger.model.MeetingCard;
import io.swagger.model.TaskCard;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

@Controller
public class MeetingsApiController implements MeetingsApi {

    private static final Logger log = LoggerFactory.getLogger(MeetingsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public MeetingsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<TaskCard> acceptTask(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "task_id", required = true) Integer taskId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TaskCard>(objectMapper.readValue("{  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}", TaskCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TaskCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TaskCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<MeetingCard> changeMeeting(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId,@ApiParam(value = ""  )  @Valid @RequestBody MeetingCard meeting) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<MeetingCard>(objectMapper.readValue("{  \"meeting_status\" : 1,  \"presenses\" : [ {    \"pres_type\" : 5,    \"user_id\" : 1  }, {    \"pres_type\" : 5,    \"user_id\" : 1  } ],  \"cmt_id\" : 0,  \"meeting_id\" : 1,  \"moderator_id\" : 6,  \"meeting_notes\" : \"\",  \"meeting_fin_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"meeting_start_dt\" : \"2000-01-23T04:56:07.000+00:00\"}", MeetingCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<MeetingCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<MeetingCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> excludeComment(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "comm_id", required = true) Integer commId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> finalizeMeeting(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<MeetingCard>> getMeeting(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<MeetingCard>>(objectMapper.readValue("[ {  \"meeting_status\" : 1,  \"presenses\" : [ {    \"pres_type\" : 5,    \"user_id\" : 1  }, {    \"pres_type\" : 5,    \"user_id\" : 1  } ],  \"cmt_id\" : 0,  \"meeting_id\" : 1,  \"moderator_id\" : 6,  \"meeting_notes\" : \"\",  \"meeting_fin_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"meeting_start_dt\" : \"2000-01-23T04:56:07.000+00:00\"}, {  \"meeting_status\" : 1,  \"presenses\" : [ {    \"pres_type\" : 5,    \"user_id\" : 1  }, {    \"pres_type\" : 5,    \"user_id\" : 1  } ],  \"cmt_id\" : 0,  \"meeting_id\" : 1,  \"moderator_id\" : 6,  \"meeting_notes\" : \"\",  \"meeting_fin_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"meeting_start_dt\" : \"2000-01-23T04:56:07.000+00:00\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<MeetingCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<MeetingCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<TaskCard>> getMeetingTasks(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<TaskCard>>(objectMapper.readValue("[ {  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}, {  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<TaskCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<TaskCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> includeComment(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "comm_id", required = true) Integer commId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> removeMeeting(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> reopenMeeting(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> startMeeting(@ApiParam(value = "" ,required=true )  @Valid @RequestBody MeetingCard meeting) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<TaskCard> unacceptTask(@ApiParam(value = "",required=true) @PathVariable("meeting_id") Integer meetingId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "task_id", required = true) Integer taskId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TaskCard>(objectMapper.readValue("{  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}", TaskCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TaskCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TaskCard>(HttpStatus.NOT_IMPLEMENTED);
    }

}
