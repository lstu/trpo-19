package io.swagger.api;

import io.swagger.model.CommentCard;
import io.swagger.model.DocumentCard;
import io.swagger.model.TaskCard;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

@Controller
public class TasksApiController implements TasksApi {

    private static final Logger log = LoggerFactory.getLogger(TasksApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public TasksApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<DocumentCard> addDocument(@ApiParam(value = "",required=true) @PathVariable("task_id") Integer taskId,@ApiParam(value = ""  )  @Valid @RequestBody DocumentCard document) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<DocumentCard>(objectMapper.readValue("{  \"doc_stat\" : 1,  \"doc_source_doc\" : 5,  \"doc_name\" : \"\",  \"task_id\" : 0,  \"doc_type\" : 6,  \"doc_id\" : 1,  \"doc_notes\" : \"\"}", DocumentCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<DocumentCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<DocumentCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<TaskCard> addTask(@ApiParam(value = "" ,required=true )  @Valid @RequestBody TaskCard task) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TaskCard>(objectMapper.readValue("{  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}", TaskCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TaskCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TaskCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<TaskCard> changeTask(@ApiParam(value = "",required=true) @PathVariable("task_id") Integer taskId,@ApiParam(value = "" ,required=true )  @Valid @RequestBody TaskCard task) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TaskCard>(objectMapper.readValue("{  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}", TaskCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TaskCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TaskCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<DocumentCard>> getDocuments(@ApiParam(value = "",required=true) @PathVariable("task_id") Integer taskId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<DocumentCard>>(objectMapper.readValue("[ {  \"doc_stat\" : 1,  \"doc_source_doc\" : 5,  \"doc_name\" : \"\",  \"task_id\" : 0,  \"doc_type\" : 6,  \"doc_id\" : 1,  \"doc_notes\" : \"\"}, {  \"doc_stat\" : 1,  \"doc_source_doc\" : 5,  \"doc_name\" : \"\",  \"task_id\" : 0,  \"doc_type\" : 6,  \"doc_id\" : 1,  \"doc_notes\" : \"\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<DocumentCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<DocumentCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<CommentCard>> getTaskComments(@ApiParam(value = "",required=true) @PathVariable("task_id") Integer taskId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<CommentCard>>(objectMapper.readValue("[ {  \"comment_text\" : \"\",  \"user_id\" : 6,  \"cmt_id\" : 5,  \"meeting_id\" : 1,  \"comm_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_id\" : 0,  \"comm_id\" : 1,  \"comment_isreply_on\" : 5}, {  \"comment_text\" : \"\",  \"user_id\" : 6,  \"cmt_id\" : 5,  \"meeting_id\" : 1,  \"comm_dt\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_id\" : 0,  \"comm_id\" : 1,  \"comment_isreply_on\" : 5} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<CommentCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<CommentCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<TaskCard>> getTasks(@ApiParam(value = "status code, not id") @Valid @RequestParam(value = "task_status", required = false) String taskStatus) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<TaskCard>>(objectMapper.readValue("[ {  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}, {  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<TaskCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<TaskCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<TaskCard> proposeTaskChange(@ApiParam(value = "",required=true) @PathVariable("task_id") Integer taskId,@ApiParam(value = ""  )  @Valid @RequestBody TaskCard task) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TaskCard>(objectMapper.readValue("{  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}", TaskCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TaskCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TaskCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<TaskCard> refuseTask(@ApiParam(value = "",required=true) @PathVariable("task_id") Integer taskId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TaskCard>(objectMapper.readValue("{  \"task_name\" : \"\",  \"task_description\" : \"\",  \"task_status\" : 1,  \"task_item_num\" : 1,  \"task_report\" : \"\",  \"task_proposed_status\" : 1,  \"parent_task_id\" : 5,  \"task_changed\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_checkup\" : \"2000-01-23T04:56:07.000+00:00\",  \"managed_by\" : 4,  \"task_code\" : \"325\",  \"task_id\" : 1,  \"cmt_id\" : 1,  \"task_deadline\" : \"2000-01-23T04:56:07.000+00:00\",  \"task_resolution\" : \"\",  \"task_type\" : 10,  \"assigned_to\" : 1}", TaskCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TaskCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TaskCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> removeTask(@ApiParam(value = "",required=true) @PathVariable("task_id") Integer taskId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
