package io.swagger.api;

import io.swagger.model.UserCard;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

@Controller
public class UsersApiController implements UsersApi {

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<UserCard> changeUser(@ApiParam(value = "",required=true) @PathVariable("user_id") Integer userId,@ApiParam(value = "" ,required=true )  @Valid @RequestBody UserCard user) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserCard>(objectMapper.readValue("{  \"user_email\" : \"user_email\",  \"user_id\" : 1,  \"user_name\" : \"Andrew Butko\",  \"user_nick\" : \"ABT\",  \"user_ad_id\" : \"user_ad_id\"}", UserCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<UserCard> getUser(@ApiParam(value = "",required=true) @PathVariable("user_id") Integer userId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserCard>(objectMapper.readValue("[ {  \"user_id\" : 1,  \"user_name\" : \"Andrew Butko\",  \"user_nick\" : \"ABT\"} ]", UserCard.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserCard>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<UserCard>> getUsers() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<UserCard>>(objectMapper.readValue("[ {  \"user_id\" : 1,  \"user_name\" : \"Andrew Butko\",  \"user_nick\" : \"ABT\"}, {  \"user_id\" : 2,  \"user_name\" : \"Juri Kamenuk\",  \"user_nick\" : \"JKA\"}, {  \"user_id\" : 3,  \"user_name\" : \"Juri Bokachev\",  \"user_nick\" : \"YUB\"}, {  \"user_id\" : 4,  \"user_name\" : \"Christian J. Weinreich\",  \"user_nick\" : \"CWE\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<UserCard>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<UserCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

}
