package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * attendance to a committee
 */
@ApiModel(description = "attendance to a committee")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class AttendanceCard   {
  @JsonProperty("user_id")
  private Integer userId = null;

  @JsonProperty("member_role")
  private Integer memberRole = null;

  @JsonProperty("email_notification")
  private Boolean emailNotification = null;

  public AttendanceCard userId(Integer userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")


  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public AttendanceCard memberRole(Integer memberRole) {
    this.memberRole = memberRole;
    return this;
  }

  /**
   * Get memberRole
   * @return memberRole
  **/
  @ApiModelProperty(value = "")


  public Integer getMemberRole() {
    return memberRole;
  }

  public void setMemberRole(Integer memberRole) {
    this.memberRole = memberRole;
  }

  public AttendanceCard emailNotification(Boolean emailNotification) {
    this.emailNotification = emailNotification;
    return this;
  }

  /**
   * Get emailNotification
   * @return emailNotification
  **/
  @ApiModelProperty(value = "")


  public Boolean isEmailNotification() {
    return emailNotification;
  }

  public void setEmailNotification(Boolean emailNotification) {
    this.emailNotification = emailNotification;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AttendanceCard attendanceCard = (AttendanceCard) o;
    return Objects.equals(this.userId, attendanceCard.userId) &&
        Objects.equals(this.memberRole, attendanceCard.memberRole) &&
        Objects.equals(this.emailNotification, attendanceCard.emailNotification);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, memberRole, emailNotification);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AttendanceCard {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    memberRole: ").append(toIndentedString(memberRole)).append("\n");
    sb.append("    emailNotification: ").append(toIndentedString(emailNotification)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

