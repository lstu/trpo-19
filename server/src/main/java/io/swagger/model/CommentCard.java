package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.Text;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * a comment to a task
 */
@ApiModel(description = "a comment to a task")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class CommentCard   {
  @JsonProperty("comm_id")
  private Integer commId = null;

  @JsonProperty("task_id")
  private Integer taskId = null;

  @JsonProperty("user_id")
  private Integer userId = null;

  @JsonProperty("meeting_id")
  private Integer meetingId = null;

  @JsonProperty("comment_isreply_on")
  private Integer commentIsreplyOn = null;

  @JsonProperty("comm_dt")
  private OffsetDateTime commDt = null;

  @JsonProperty("comment_text")
  private Text commentText = null;

  @JsonProperty("cmt_id")
  private Integer cmtId = null;

  public CommentCard commId(Integer commId) {
    this.commId = commId;
    return this;
  }

  /**
   * Get commId
   * @return commId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getCommId() {
    return commId;
  }

  public void setCommId(Integer commId) {
    this.commId = commId;
  }

  public CommentCard taskId(Integer taskId) {
    this.taskId = taskId;
    return this;
  }

  /**
   * Get taskId
   * @return taskId
  **/
  @ApiModelProperty(value = "")


  public Integer getTaskId() {
    return taskId;
  }

  public void setTaskId(Integer taskId) {
    this.taskId = taskId;
  }

  public CommentCard userId(Integer userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")


  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public CommentCard meetingId(Integer meetingId) {
    this.meetingId = meetingId;
    return this;
  }

  /**
   * Get meetingId
   * @return meetingId
  **/
  @ApiModelProperty(value = "")


  public Integer getMeetingId() {
    return meetingId;
  }

  public void setMeetingId(Integer meetingId) {
    this.meetingId = meetingId;
  }

  public CommentCard commentIsreplyOn(Integer commentIsreplyOn) {
    this.commentIsreplyOn = commentIsreplyOn;
    return this;
  }

  /**
   * Get commentIsreplyOn
   * @return commentIsreplyOn
  **/
  @ApiModelProperty(value = "")


  public Integer getCommentIsreplyOn() {
    return commentIsreplyOn;
  }

  public void setCommentIsreplyOn(Integer commentIsreplyOn) {
    this.commentIsreplyOn = commentIsreplyOn;
  }

  public CommentCard commDt(OffsetDateTime commDt) {
    this.commDt = commDt;
    return this;
  }

  /**
   * Get commDt
   * @return commDt
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getCommDt() {
    return commDt;
  }

  public void setCommDt(OffsetDateTime commDt) {
    this.commDt = commDt;
  }

  public CommentCard commentText(Text commentText) {
    this.commentText = commentText;
    return this;
  }

  /**
   * Get commentText
   * @return commentText
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getCommentText() {
    return commentText;
  }

  public void setCommentText(Text commentText) {
    this.commentText = commentText;
  }

  public CommentCard cmtId(Integer cmtId) {
    this.cmtId = cmtId;
    return this;
  }

  /**
   * Get cmtId
   * @return cmtId
  **/
  @ApiModelProperty(value = "")


  public Integer getCmtId() {
    return cmtId;
  }

  public void setCmtId(Integer cmtId) {
    this.cmtId = cmtId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommentCard commentCard = (CommentCard) o;
    return Objects.equals(this.commId, commentCard.commId) &&
        Objects.equals(this.taskId, commentCard.taskId) &&
        Objects.equals(this.userId, commentCard.userId) &&
        Objects.equals(this.meetingId, commentCard.meetingId) &&
        Objects.equals(this.commentIsreplyOn, commentCard.commentIsreplyOn) &&
        Objects.equals(this.commDt, commentCard.commDt) &&
        Objects.equals(this.commentText, commentCard.commentText) &&
        Objects.equals(this.cmtId, commentCard.cmtId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(commId, taskId, userId, meetingId, commentIsreplyOn, commDt, commentText, cmtId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommentCard {\n");
    
    sb.append("    commId: ").append(toIndentedString(commId)).append("\n");
    sb.append("    taskId: ").append(toIndentedString(taskId)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    meetingId: ").append(toIndentedString(meetingId)).append("\n");
    sb.append("    commentIsreplyOn: ").append(toIndentedString(commentIsreplyOn)).append("\n");
    sb.append("    commDt: ").append(toIndentedString(commDt)).append("\n");
    sb.append("    commentText: ").append(toIndentedString(commentText)).append("\n");
    sb.append("    cmtId: ").append(toIndentedString(cmtId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

