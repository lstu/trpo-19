package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.AttendanceCard;
import io.swagger.model.Text;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommitteeCard
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class CommitteeCard   {
  @JsonProperty("cmt_id")
  private Integer cmtId = null;

  @JsonProperty("cmt_code")
  private String cmtCode = null;

  @JsonProperty("cmt_status")
  private Integer cmtStatus = null;

  @JsonProperty("cmt_pattern")
  private String cmtPattern = null;

  @JsonProperty("cmt_next_dt")
  private OffsetDateTime cmtNextDt = null;

  @JsonProperty("cmt_location")
  private String cmtLocation = null;

  @JsonProperty("cmt_name")
  private Text cmtName = null;

  @JsonProperty("cmt_topic")
  private Text cmtTopic = null;

  @JsonProperty("cmt_announcement")
  private Text cmtAnnouncement = null;

  @JsonProperty("attendances")
  @Valid
  private List<AttendanceCard> attendances = null;

  public CommitteeCard cmtId(Integer cmtId) {
    this.cmtId = cmtId;
    return this;
  }

  /**
   * Get cmtId
   * @return cmtId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getCmtId() {
    return cmtId;
  }

  public void setCmtId(Integer cmtId) {
    this.cmtId = cmtId;
  }

  public CommitteeCard cmtCode(String cmtCode) {
    this.cmtCode = cmtCode;
    return this;
  }

  /**
   * Get cmtCode
   * @return cmtCode
  **/
  @ApiModelProperty(example = "QUALITY", value = "")


  public String getCmtCode() {
    return cmtCode;
  }

  public void setCmtCode(String cmtCode) {
    this.cmtCode = cmtCode;
  }

  public CommitteeCard cmtStatus(Integer cmtStatus) {
    this.cmtStatus = cmtStatus;
    return this;
  }

  /**
   * Get cmtStatus
   * @return cmtStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getCmtStatus() {
    return cmtStatus;
  }

  public void setCmtStatus(Integer cmtStatus) {
    this.cmtStatus = cmtStatus;
  }

  public CommitteeCard cmtPattern(String cmtPattern) {
    this.cmtPattern = cmtPattern;
    return this;
  }

  /**
   * Get cmtPattern
   * @return cmtPattern
  **/
  @ApiModelProperty(value = "")


  public String getCmtPattern() {
    return cmtPattern;
  }

  public void setCmtPattern(String cmtPattern) {
    this.cmtPattern = cmtPattern;
  }

  public CommitteeCard cmtNextDt(OffsetDateTime cmtNextDt) {
    this.cmtNextDt = cmtNextDt;
    return this;
  }

  /**
   * Get cmtNextDt
   * @return cmtNextDt
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getCmtNextDt() {
    return cmtNextDt;
  }

  public void setCmtNextDt(OffsetDateTime cmtNextDt) {
    this.cmtNextDt = cmtNextDt;
  }

  public CommitteeCard cmtLocation(String cmtLocation) {
    this.cmtLocation = cmtLocation;
    return this;
  }

  /**
   * Get cmtLocation
   * @return cmtLocation
  **/
  @ApiModelProperty(value = "")


  public String getCmtLocation() {
    return cmtLocation;
  }

  public void setCmtLocation(String cmtLocation) {
    this.cmtLocation = cmtLocation;
  }

  public CommitteeCard cmtName(Text cmtName) {
    this.cmtName = cmtName;
    return this;
  }

  /**
   * Get cmtName
   * @return cmtName
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getCmtName() {
    return cmtName;
  }

  public void setCmtName(Text cmtName) {
    this.cmtName = cmtName;
  }

  public CommitteeCard cmtTopic(Text cmtTopic) {
    this.cmtTopic = cmtTopic;
    return this;
  }

  /**
   * Get cmtTopic
   * @return cmtTopic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getCmtTopic() {
    return cmtTopic;
  }

  public void setCmtTopic(Text cmtTopic) {
    this.cmtTopic = cmtTopic;
  }

  public CommitteeCard cmtAnnouncement(Text cmtAnnouncement) {
    this.cmtAnnouncement = cmtAnnouncement;
    return this;
  }

  /**
   * Get cmtAnnouncement
   * @return cmtAnnouncement
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getCmtAnnouncement() {
    return cmtAnnouncement;
  }

  public void setCmtAnnouncement(Text cmtAnnouncement) {
    this.cmtAnnouncement = cmtAnnouncement;
  }

  public CommitteeCard attendances(List<AttendanceCard> attendances) {
    this.attendances = attendances;
    return this;
  }

  public CommitteeCard addAttendancesItem(AttendanceCard attendancesItem) {
    if (this.attendances == null) {
      this.attendances = new ArrayList<AttendanceCard>();
    }
    this.attendances.add(attendancesItem);
    return this;
  }

  /**
   * Get attendances
   * @return attendances
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<AttendanceCard> getAttendances() {
    return attendances;
  }

  public void setAttendances(List<AttendanceCard> attendances) {
    this.attendances = attendances;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommitteeCard committeeCard = (CommitteeCard) o;
    return Objects.equals(this.cmtId, committeeCard.cmtId) &&
        Objects.equals(this.cmtCode, committeeCard.cmtCode) &&
        Objects.equals(this.cmtStatus, committeeCard.cmtStatus) &&
        Objects.equals(this.cmtPattern, committeeCard.cmtPattern) &&
        Objects.equals(this.cmtNextDt, committeeCard.cmtNextDt) &&
        Objects.equals(this.cmtLocation, committeeCard.cmtLocation) &&
        Objects.equals(this.cmtName, committeeCard.cmtName) &&
        Objects.equals(this.cmtTopic, committeeCard.cmtTopic) &&
        Objects.equals(this.cmtAnnouncement, committeeCard.cmtAnnouncement) &&
        Objects.equals(this.attendances, committeeCard.attendances);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cmtId, cmtCode, cmtStatus, cmtPattern, cmtNextDt, cmtLocation, cmtName, cmtTopic, cmtAnnouncement, attendances);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommitteeCard {\n");
    
    sb.append("    cmtId: ").append(toIndentedString(cmtId)).append("\n");
    sb.append("    cmtCode: ").append(toIndentedString(cmtCode)).append("\n");
    sb.append("    cmtStatus: ").append(toIndentedString(cmtStatus)).append("\n");
    sb.append("    cmtPattern: ").append(toIndentedString(cmtPattern)).append("\n");
    sb.append("    cmtNextDt: ").append(toIndentedString(cmtNextDt)).append("\n");
    sb.append("    cmtLocation: ").append(toIndentedString(cmtLocation)).append("\n");
    sb.append("    cmtName: ").append(toIndentedString(cmtName)).append("\n");
    sb.append("    cmtTopic: ").append(toIndentedString(cmtTopic)).append("\n");
    sb.append("    cmtAnnouncement: ").append(toIndentedString(cmtAnnouncement)).append("\n");
    sb.append("    attendances: ").append(toIndentedString(attendances)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

