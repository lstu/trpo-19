package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.Text;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DocumentCard
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class DocumentCard   {
  @JsonProperty("doc_id")
  private Integer docId = null;

  @JsonProperty("task_id")
  private Integer taskId = null;

  @JsonProperty("doc_type")
  private Integer docType = null;

  @JsonProperty("doc_stat")
  private Integer docStat = null;

  @JsonProperty("doc_source_doc")
  private Integer docSourceDoc = null;

  @JsonProperty("doc_name")
  private Text docName = null;

  @JsonProperty("doc_notes")
  private Text docNotes = null;

  public DocumentCard docId(Integer docId) {
    this.docId = docId;
    return this;
  }

  /**
   * Get docId
   * @return docId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getDocId() {
    return docId;
  }

  public void setDocId(Integer docId) {
    this.docId = docId;
  }

  public DocumentCard taskId(Integer taskId) {
    this.taskId = taskId;
    return this;
  }

  /**
   * Get taskId
   * @return taskId
  **/
  @ApiModelProperty(value = "")


  public Integer getTaskId() {
    return taskId;
  }

  public void setTaskId(Integer taskId) {
    this.taskId = taskId;
  }

  public DocumentCard docType(Integer docType) {
    this.docType = docType;
    return this;
  }

  /**
   * Get docType
   * @return docType
  **/
  @ApiModelProperty(value = "")


  public Integer getDocType() {
    return docType;
  }

  public void setDocType(Integer docType) {
    this.docType = docType;
  }

  public DocumentCard docStat(Integer docStat) {
    this.docStat = docStat;
    return this;
  }

  /**
   * Get docStat
   * @return docStat
  **/
  @ApiModelProperty(value = "")


  public Integer getDocStat() {
    return docStat;
  }

  public void setDocStat(Integer docStat) {
    this.docStat = docStat;
  }

  public DocumentCard docSourceDoc(Integer docSourceDoc) {
    this.docSourceDoc = docSourceDoc;
    return this;
  }

  /**
   * Get docSourceDoc
   * @return docSourceDoc
  **/
  @ApiModelProperty(value = "")


  public Integer getDocSourceDoc() {
    return docSourceDoc;
  }

  public void setDocSourceDoc(Integer docSourceDoc) {
    this.docSourceDoc = docSourceDoc;
  }

  public DocumentCard docName(Text docName) {
    this.docName = docName;
    return this;
  }

  /**
   * Get docName
   * @return docName
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getDocName() {
    return docName;
  }

  public void setDocName(Text docName) {
    this.docName = docName;
  }

  public DocumentCard docNotes(Text docNotes) {
    this.docNotes = docNotes;
    return this;
  }

  /**
   * Get docNotes
   * @return docNotes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getDocNotes() {
    return docNotes;
  }

  public void setDocNotes(Text docNotes) {
    this.docNotes = docNotes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DocumentCard documentCard = (DocumentCard) o;
    return Objects.equals(this.docId, documentCard.docId) &&
        Objects.equals(this.taskId, documentCard.taskId) &&
        Objects.equals(this.docType, documentCard.docType) &&
        Objects.equals(this.docStat, documentCard.docStat) &&
        Objects.equals(this.docSourceDoc, documentCard.docSourceDoc) &&
        Objects.equals(this.docName, documentCard.docName) &&
        Objects.equals(this.docNotes, documentCard.docNotes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docId, taskId, docType, docStat, docSourceDoc, docName, docNotes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DocumentCard {\n");
    
    sb.append("    docId: ").append(toIndentedString(docId)).append("\n");
    sb.append("    taskId: ").append(toIndentedString(taskId)).append("\n");
    sb.append("    docType: ").append(toIndentedString(docType)).append("\n");
    sb.append("    docStat: ").append(toIndentedString(docStat)).append("\n");
    sb.append("    docSourceDoc: ").append(toIndentedString(docSourceDoc)).append("\n");
    sb.append("    docName: ").append(toIndentedString(docName)).append("\n");
    sb.append("    docNotes: ").append(toIndentedString(docNotes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

