package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.DomainValue;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Domain
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class Domain   {
  @JsonProperty("dom_id")
  private Integer domId = null;

  @JsonProperty("uom_id")
  private Integer uomId = null;

  @JsonProperty("dom_type")
  private Integer domType = null;

  @JsonProperty("dom_code")
  private String domCode = null;

  @JsonProperty("dom_abbr")
  private String domAbbr = null;

  @JsonProperty("dom_name")
  private String domName = null;

  @JsonProperty("el_type_dom")
  private Integer elTypeDom = null;

  @JsonProperty("props")
  private String props = null;

  @JsonProperty("values")
  @Valid
  private List<DomainValue> values = null;

  public Domain domId(Integer domId) {
    this.domId = domId;
    return this;
  }

  /**
   * Get domId
   * @return domId
  **/
  @ApiModelProperty(value = "")


  public Integer getDomId() {
    return domId;
  }

  public void setDomId(Integer domId) {
    this.domId = domId;
  }

  public Domain uomId(Integer uomId) {
    this.uomId = uomId;
    return this;
  }

  /**
   * Get uomId
   * @return uomId
  **/
  @ApiModelProperty(value = "")


  public Integer getUomId() {
    return uomId;
  }

  public void setUomId(Integer uomId) {
    this.uomId = uomId;
  }

  public Domain domType(Integer domType) {
    this.domType = domType;
    return this;
  }

  /**
   * Get domType
   * @return domType
  **/
  @ApiModelProperty(value = "")


  public Integer getDomType() {
    return domType;
  }

  public void setDomType(Integer domType) {
    this.domType = domType;
  }

  public Domain domCode(String domCode) {
    this.domCode = domCode;
    return this;
  }

  /**
   * Get domCode
   * @return domCode
  **/
  @ApiModelProperty(value = "")


  public String getDomCode() {
    return domCode;
  }

  public void setDomCode(String domCode) {
    this.domCode = domCode;
  }

  public Domain domAbbr(String domAbbr) {
    this.domAbbr = domAbbr;
    return this;
  }

  /**
   * Get domAbbr
   * @return domAbbr
  **/
  @ApiModelProperty(value = "")


  public String getDomAbbr() {
    return domAbbr;
  }

  public void setDomAbbr(String domAbbr) {
    this.domAbbr = domAbbr;
  }

  public Domain domName(String domName) {
    this.domName = domName;
    return this;
  }

  /**
   * Get domName
   * @return domName
  **/
  @ApiModelProperty(value = "")


  public String getDomName() {
    return domName;
  }

  public void setDomName(String domName) {
    this.domName = domName;
  }

  public Domain elTypeDom(Integer elTypeDom) {
    this.elTypeDom = elTypeDom;
    return this;
  }

  /**
   * Get elTypeDom
   * @return elTypeDom
  **/
  @ApiModelProperty(value = "")


  public Integer getElTypeDom() {
    return elTypeDom;
  }

  public void setElTypeDom(Integer elTypeDom) {
    this.elTypeDom = elTypeDom;
  }

  public Domain props(String props) {
    this.props = props;
    return this;
  }

  /**
   * Get props
   * @return props
  **/
  @ApiModelProperty(value = "")


  public String getProps() {
    return props;
  }

  public void setProps(String props) {
    this.props = props;
  }

  public Domain values(List<DomainValue> values) {
    this.values = values;
    return this;
  }

  public Domain addValuesItem(DomainValue valuesItem) {
    if (this.values == null) {
      this.values = new ArrayList<DomainValue>();
    }
    this.values.add(valuesItem);
    return this;
  }

  /**
   * Get values
   * @return values
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<DomainValue> getValues() {
    return values;
  }

  public void setValues(List<DomainValue> values) {
    this.values = values;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Domain domain = (Domain) o;
    return Objects.equals(this.domId, domain.domId) &&
        Objects.equals(this.uomId, domain.uomId) &&
        Objects.equals(this.domType, domain.domType) &&
        Objects.equals(this.domCode, domain.domCode) &&
        Objects.equals(this.domAbbr, domain.domAbbr) &&
        Objects.equals(this.domName, domain.domName) &&
        Objects.equals(this.elTypeDom, domain.elTypeDom) &&
        Objects.equals(this.props, domain.props) &&
        Objects.equals(this.values, domain.values);
  }

  @Override
  public int hashCode() {
    return Objects.hash(domId, uomId, domType, domCode, domAbbr, domName, elTypeDom, props, values);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Domain {\n");
    
    sb.append("    domId: ").append(toIndentedString(domId)).append("\n");
    sb.append("    uomId: ").append(toIndentedString(uomId)).append("\n");
    sb.append("    domType: ").append(toIndentedString(domType)).append("\n");
    sb.append("    domCode: ").append(toIndentedString(domCode)).append("\n");
    sb.append("    domAbbr: ").append(toIndentedString(domAbbr)).append("\n");
    sb.append("    domName: ").append(toIndentedString(domName)).append("\n");
    sb.append("    elTypeDom: ").append(toIndentedString(elTypeDom)).append("\n");
    sb.append("    props: ").append(toIndentedString(props)).append("\n");
    sb.append("    values: ").append(toIndentedString(values)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

