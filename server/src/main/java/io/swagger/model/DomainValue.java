package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DomainValue
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class DomainValue   {
  @JsonProperty("val_id")
  private Integer valId = null;

  @JsonProperty("dom_id")
  private Integer domId = null;

  /**
   * Gets or Sets valStatus
   */
  public enum ValStatusEnum {
    N("N"),
    
    A("A"),
    
    C("C"),
    
    R("R");

    private String value;

    ValStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ValStatusEnum fromValue(String text) {
      for (ValStatusEnum b : ValStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("val_status")
  private ValStatusEnum valStatus = null;

  @JsonProperty("val_code")
  private String valCode = null;

  @JsonProperty("val_abbr")
  private String valAbbr = null;

  @JsonProperty("val_name")
  private String valName = null;

  @JsonProperty("met_val_id")
  private Integer metValId = null;

  @JsonProperty("val_el_type")
  private Integer valElType = null;

  @JsonProperty("props")
  private String props = null;

  public DomainValue valId(Integer valId) {
    this.valId = valId;
    return this;
  }

  /**
   * Get valId
   * @return valId
  **/
  @ApiModelProperty(value = "")


  public Integer getValId() {
    return valId;
  }

  public void setValId(Integer valId) {
    this.valId = valId;
  }

  public DomainValue domId(Integer domId) {
    this.domId = domId;
    return this;
  }

  /**
   * Get domId
   * @return domId
  **/
  @ApiModelProperty(value = "")


  public Integer getDomId() {
    return domId;
  }

  public void setDomId(Integer domId) {
    this.domId = domId;
  }

  public DomainValue valStatus(ValStatusEnum valStatus) {
    this.valStatus = valStatus;
    return this;
  }

  /**
   * Get valStatus
   * @return valStatus
  **/
  @ApiModelProperty(value = "")


  public ValStatusEnum getValStatus() {
    return valStatus;
  }

  public void setValStatus(ValStatusEnum valStatus) {
    this.valStatus = valStatus;
  }

  public DomainValue valCode(String valCode) {
    this.valCode = valCode;
    return this;
  }

  /**
   * Get valCode
   * @return valCode
  **/
  @ApiModelProperty(value = "")


  public String getValCode() {
    return valCode;
  }

  public void setValCode(String valCode) {
    this.valCode = valCode;
  }

  public DomainValue valAbbr(String valAbbr) {
    this.valAbbr = valAbbr;
    return this;
  }

  /**
   * Get valAbbr
   * @return valAbbr
  **/
  @ApiModelProperty(value = "")


  public String getValAbbr() {
    return valAbbr;
  }

  public void setValAbbr(String valAbbr) {
    this.valAbbr = valAbbr;
  }

  public DomainValue valName(String valName) {
    this.valName = valName;
    return this;
  }

  /**
   * Get valName
   * @return valName
  **/
  @ApiModelProperty(value = "")


  public String getValName() {
    return valName;
  }

  public void setValName(String valName) {
    this.valName = valName;
  }

  public DomainValue metValId(Integer metValId) {
    this.metValId = metValId;
    return this;
  }

  /**
   * Get metValId
   * @return metValId
  **/
  @ApiModelProperty(value = "")


  public Integer getMetValId() {
    return metValId;
  }

  public void setMetValId(Integer metValId) {
    this.metValId = metValId;
  }

  public DomainValue valElType(Integer valElType) {
    this.valElType = valElType;
    return this;
  }

  /**
   * Get valElType
   * @return valElType
  **/
  @ApiModelProperty(value = "")


  public Integer getValElType() {
    return valElType;
  }

  public void setValElType(Integer valElType) {
    this.valElType = valElType;
  }

  public DomainValue props(String props) {
    this.props = props;
    return this;
  }

  /**
   * Get props
   * @return props
  **/
  @ApiModelProperty(value = "")


  public String getProps() {
    return props;
  }

  public void setProps(String props) {
    this.props = props;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DomainValue domainValue = (DomainValue) o;
    return Objects.equals(this.valId, domainValue.valId) &&
        Objects.equals(this.domId, domainValue.domId) &&
        Objects.equals(this.valStatus, domainValue.valStatus) &&
        Objects.equals(this.valCode, domainValue.valCode) &&
        Objects.equals(this.valAbbr, domainValue.valAbbr) &&
        Objects.equals(this.valName, domainValue.valName) &&
        Objects.equals(this.metValId, domainValue.metValId) &&
        Objects.equals(this.valElType, domainValue.valElType) &&
        Objects.equals(this.props, domainValue.props);
  }

  @Override
  public int hashCode() {
    return Objects.hash(valId, domId, valStatus, valCode, valAbbr, valName, metValId, valElType, props);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DomainValue {\n");
    
    sb.append("    valId: ").append(toIndentedString(valId)).append("\n");
    sb.append("    domId: ").append(toIndentedString(domId)).append("\n");
    sb.append("    valStatus: ").append(toIndentedString(valStatus)).append("\n");
    sb.append("    valCode: ").append(toIndentedString(valCode)).append("\n");
    sb.append("    valAbbr: ").append(toIndentedString(valAbbr)).append("\n");
    sb.append("    valName: ").append(toIndentedString(valName)).append("\n");
    sb.append("    metValId: ").append(toIndentedString(metValId)).append("\n");
    sb.append("    valElType: ").append(toIndentedString(valElType)).append("\n");
    sb.append("    props: ").append(toIndentedString(props)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

