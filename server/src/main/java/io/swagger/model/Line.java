package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * One international string value for a specific language
 */
@ApiModel(description = "One international string value for a specific language")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class Line   {
  /**
   * Gets or Sets langCode
   */
  public enum LangCodeEnum {
    RU("RU"),
    
    EN("EN"),
    
    DK("DK");

    private String value;

    LangCodeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static LangCodeEnum fromValue(String text) {
      for (LangCodeEnum b : LangCodeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("lang_code")
  private LangCodeEnum langCode = null;

  @JsonProperty("string")
  private String string = null;

  public Line langCode(LangCodeEnum langCode) {
    this.langCode = langCode;
    return this;
  }

  /**
   * Get langCode
   * @return langCode
  **/
  @ApiModelProperty(example = "RU", value = "")


  public LangCodeEnum getLangCode() {
    return langCode;
  }

  public void setLangCode(LangCodeEnum langCode) {
    this.langCode = langCode;
  }

  public Line string(String string) {
    this.string = string;
    return this;
  }

  /**
   * Get string
   * @return string
  **/
  @ApiModelProperty(example = "Комментарий", value = "")


  public String getString() {
    return string;
  }

  public void setString(String string) {
    this.string = string;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Line line = (Line) o;
    return Objects.equals(this.langCode, line.langCode) &&
        Objects.equals(this.string, line.string);
  }

  @Override
  public int hashCode() {
    return Objects.hash(langCode, string);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Line {\n");
    
    sb.append("    langCode: ").append(toIndentedString(langCode)).append("\n");
    sb.append("    string: ").append(toIndentedString(string)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

