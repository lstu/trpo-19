package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.PresenceCard;
import io.swagger.model.Text;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MeetingCard
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class MeetingCard   {
  @JsonProperty("meeting_id")
  private Integer meetingId = null;

  @JsonProperty("cmt_id")
  private Integer cmtId = null;

  @JsonProperty("meeting_status")
  private Integer meetingStatus = null;

  @JsonProperty("moderator_id")
  private Integer moderatorId = null;

  @JsonProperty("meeting_start_dt")
  private OffsetDateTime meetingStartDt = null;

  @JsonProperty("meeting_fin_dt")
  private OffsetDateTime meetingFinDt = null;

  @JsonProperty("meeting_notes")
  private Text meetingNotes = null;

  @JsonProperty("presenses")
  @Valid
  private List<PresenceCard> presenses = null;

  public MeetingCard meetingId(Integer meetingId) {
    this.meetingId = meetingId;
    return this;
  }

  /**
   * Get meetingId
   * @return meetingId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getMeetingId() {
    return meetingId;
  }

  public void setMeetingId(Integer meetingId) {
    this.meetingId = meetingId;
  }

  public MeetingCard cmtId(Integer cmtId) {
    this.cmtId = cmtId;
    return this;
  }

  /**
   * Get cmtId
   * @return cmtId
  **/
  @ApiModelProperty(value = "")


  public Integer getCmtId() {
    return cmtId;
  }

  public void setCmtId(Integer cmtId) {
    this.cmtId = cmtId;
  }

  public MeetingCard meetingStatus(Integer meetingStatus) {
    this.meetingStatus = meetingStatus;
    return this;
  }

  /**
   * Get meetingStatus
   * @return meetingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getMeetingStatus() {
    return meetingStatus;
  }

  public void setMeetingStatus(Integer meetingStatus) {
    this.meetingStatus = meetingStatus;
  }

  public MeetingCard moderatorId(Integer moderatorId) {
    this.moderatorId = moderatorId;
    return this;
  }

  /**
   * Get moderatorId
   * @return moderatorId
  **/
  @ApiModelProperty(value = "")


  public Integer getModeratorId() {
    return moderatorId;
  }

  public void setModeratorId(Integer moderatorId) {
    this.moderatorId = moderatorId;
  }

  public MeetingCard meetingStartDt(OffsetDateTime meetingStartDt) {
    this.meetingStartDt = meetingStartDt;
    return this;
  }

  /**
   * Get meetingStartDt
   * @return meetingStartDt
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getMeetingStartDt() {
    return meetingStartDt;
  }

  public void setMeetingStartDt(OffsetDateTime meetingStartDt) {
    this.meetingStartDt = meetingStartDt;
  }

  public MeetingCard meetingFinDt(OffsetDateTime meetingFinDt) {
    this.meetingFinDt = meetingFinDt;
    return this;
  }

  /**
   * Get meetingFinDt
   * @return meetingFinDt
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getMeetingFinDt() {
    return meetingFinDt;
  }

  public void setMeetingFinDt(OffsetDateTime meetingFinDt) {
    this.meetingFinDt = meetingFinDt;
  }

  public MeetingCard meetingNotes(Text meetingNotes) {
    this.meetingNotes = meetingNotes;
    return this;
  }

  /**
   * Get meetingNotes
   * @return meetingNotes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getMeetingNotes() {
    return meetingNotes;
  }

  public void setMeetingNotes(Text meetingNotes) {
    this.meetingNotes = meetingNotes;
  }

  public MeetingCard presenses(List<PresenceCard> presenses) {
    this.presenses = presenses;
    return this;
  }

  public MeetingCard addPresensesItem(PresenceCard presensesItem) {
    if (this.presenses == null) {
      this.presenses = new ArrayList<PresenceCard>();
    }
    this.presenses.add(presensesItem);
    return this;
  }

  /**
   * Get presenses
   * @return presenses
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PresenceCard> getPresenses() {
    return presenses;
  }

  public void setPresenses(List<PresenceCard> presenses) {
    this.presenses = presenses;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MeetingCard meetingCard = (MeetingCard) o;
    return Objects.equals(this.meetingId, meetingCard.meetingId) &&
        Objects.equals(this.cmtId, meetingCard.cmtId) &&
        Objects.equals(this.meetingStatus, meetingCard.meetingStatus) &&
        Objects.equals(this.moderatorId, meetingCard.moderatorId) &&
        Objects.equals(this.meetingStartDt, meetingCard.meetingStartDt) &&
        Objects.equals(this.meetingFinDt, meetingCard.meetingFinDt) &&
        Objects.equals(this.meetingNotes, meetingCard.meetingNotes) &&
        Objects.equals(this.presenses, meetingCard.presenses);
  }

  @Override
  public int hashCode() {
    return Objects.hash(meetingId, cmtId, meetingStatus, moderatorId, meetingStartDt, meetingFinDt, meetingNotes, presenses);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MeetingCard {\n");
    
    sb.append("    meetingId: ").append(toIndentedString(meetingId)).append("\n");
    sb.append("    cmtId: ").append(toIndentedString(cmtId)).append("\n");
    sb.append("    meetingStatus: ").append(toIndentedString(meetingStatus)).append("\n");
    sb.append("    moderatorId: ").append(toIndentedString(moderatorId)).append("\n");
    sb.append("    meetingStartDt: ").append(toIndentedString(meetingStartDt)).append("\n");
    sb.append("    meetingFinDt: ").append(toIndentedString(meetingFinDt)).append("\n");
    sb.append("    meetingNotes: ").append(toIndentedString(meetingNotes)).append("\n");
    sb.append("    presenses: ").append(toIndentedString(presenses)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

