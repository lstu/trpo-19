package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * presence to a meeting
 */
@ApiModel(description = "presence to a meeting")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class PresenceCard   {
  @JsonProperty("user_id")
  private Integer userId = null;

  @JsonProperty("pres_type")
  private Integer presType = null;

  public PresenceCard userId(Integer userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")


  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public PresenceCard presType(Integer presType) {
    this.presType = presType;
    return this;
  }

  /**
   * Get presType
   * @return presType
  **/
  @ApiModelProperty(value = "")


  public Integer getPresType() {
    return presType;
  }

  public void setPresType(Integer presType) {
    this.presType = presType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PresenceCard presenceCard = (PresenceCard) o;
    return Objects.equals(this.userId, presenceCard.userId) &&
        Objects.equals(this.presType, presenceCard.presType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, presType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PresenceCard {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    presType: ").append(toIndentedString(presType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

