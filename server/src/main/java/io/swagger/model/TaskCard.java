package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.Text;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TaskCard
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class TaskCard   {
  @JsonProperty("task_id")
  private Integer taskId = null;

  @JsonProperty("task_type")
  private Integer taskType = null;

  @JsonProperty("task_code")
  private String taskCode = null;

  @JsonProperty("task_status")
  private Integer taskStatus = null;

  @JsonProperty("task_proposed_status")
  private Integer taskProposedStatus = null;

  @JsonProperty("cmt_id")
  private Integer cmtId = null;

  @JsonProperty("assigned_to")
  private Integer assignedTo = null;

  @JsonProperty("managed_by")
  private Integer managedBy = null;

  @JsonProperty("parent_task_id")
  private Integer parentTaskId = null;

  @JsonProperty("task_checkup")
  private OffsetDateTime taskCheckup = null;

  @JsonProperty("task_deadline")
  private OffsetDateTime taskDeadline = null;

  @JsonProperty("task_item_num")
  private Integer taskItemNum = null;

  @JsonProperty("task_name")
  private Text taskName = null;

  @JsonProperty("task_description")
  private Text taskDescription = null;

  @JsonProperty("task_report")
  private Text taskReport = null;

  @JsonProperty("task_resolution")
  private Text taskResolution = null;

  @JsonProperty("task_changed")
  private OffsetDateTime taskChanged = null;

  public TaskCard taskId(Integer taskId) {
    this.taskId = taskId;
    return this;
  }

  /**
   * Get taskId
   * @return taskId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getTaskId() {
    return taskId;
  }

  public void setTaskId(Integer taskId) {
    this.taskId = taskId;
  }

  public TaskCard taskType(Integer taskType) {
    this.taskType = taskType;
    return this;
  }

  /**
   * Get taskType
   * @return taskType
  **/
  @ApiModelProperty(example = "10", value = "")


  public Integer getTaskType() {
    return taskType;
  }

  public void setTaskType(Integer taskType) {
    this.taskType = taskType;
  }

  public TaskCard taskCode(String taskCode) {
    this.taskCode = taskCode;
    return this;
  }

  /**
   * Get taskCode
   * @return taskCode
  **/
  @ApiModelProperty(example = "325", value = "")


  public String getTaskCode() {
    return taskCode;
  }

  public void setTaskCode(String taskCode) {
    this.taskCode = taskCode;
  }

  public TaskCard taskStatus(Integer taskStatus) {
    this.taskStatus = taskStatus;
    return this;
  }

  /**
   * Get taskStatus
   * @return taskStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getTaskStatus() {
    return taskStatus;
  }

  public void setTaskStatus(Integer taskStatus) {
    this.taskStatus = taskStatus;
  }

  public TaskCard taskProposedStatus(Integer taskProposedStatus) {
    this.taskProposedStatus = taskProposedStatus;
    return this;
  }

  /**
   * Get taskProposedStatus
   * @return taskProposedStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getTaskProposedStatus() {
    return taskProposedStatus;
  }

  public void setTaskProposedStatus(Integer taskProposedStatus) {
    this.taskProposedStatus = taskProposedStatus;
  }

  public TaskCard cmtId(Integer cmtId) {
    this.cmtId = cmtId;
    return this;
  }

  /**
   * Get cmtId
   * @return cmtId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getCmtId() {
    return cmtId;
  }

  public void setCmtId(Integer cmtId) {
    this.cmtId = cmtId;
  }

  public TaskCard assignedTo(Integer assignedTo) {
    this.assignedTo = assignedTo;
    return this;
  }

  /**
   * Get assignedTo
   * @return assignedTo
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getAssignedTo() {
    return assignedTo;
  }

  public void setAssignedTo(Integer assignedTo) {
    this.assignedTo = assignedTo;
  }

  public TaskCard managedBy(Integer managedBy) {
    this.managedBy = managedBy;
    return this;
  }

  /**
   * Get managedBy
   * @return managedBy
  **/
  @ApiModelProperty(example = "4", value = "")


  public Integer getManagedBy() {
    return managedBy;
  }

  public void setManagedBy(Integer managedBy) {
    this.managedBy = managedBy;
  }

  public TaskCard parentTaskId(Integer parentTaskId) {
    this.parentTaskId = parentTaskId;
    return this;
  }

  /**
   * Get parentTaskId
   * @return parentTaskId
  **/
  @ApiModelProperty(example = "5", value = "")


  public Integer getParentTaskId() {
    return parentTaskId;
  }

  public void setParentTaskId(Integer parentTaskId) {
    this.parentTaskId = parentTaskId;
  }

  public TaskCard taskCheckup(OffsetDateTime taskCheckup) {
    this.taskCheckup = taskCheckup;
    return this;
  }

  /**
   * Get taskCheckup
   * @return taskCheckup
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getTaskCheckup() {
    return taskCheckup;
  }

  public void setTaskCheckup(OffsetDateTime taskCheckup) {
    this.taskCheckup = taskCheckup;
  }

  public TaskCard taskDeadline(OffsetDateTime taskDeadline) {
    this.taskDeadline = taskDeadline;
    return this;
  }

  /**
   * Get taskDeadline
   * @return taskDeadline
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getTaskDeadline() {
    return taskDeadline;
  }

  public void setTaskDeadline(OffsetDateTime taskDeadline) {
    this.taskDeadline = taskDeadline;
  }

  public TaskCard taskItemNum(Integer taskItemNum) {
    this.taskItemNum = taskItemNum;
    return this;
  }

  /**
   * Get taskItemNum
   * @return taskItemNum
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getTaskItemNum() {
    return taskItemNum;
  }

  public void setTaskItemNum(Integer taskItemNum) {
    this.taskItemNum = taskItemNum;
  }

  public TaskCard taskName(Text taskName) {
    this.taskName = taskName;
    return this;
  }

  /**
   * Get taskName
   * @return taskName
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getTaskName() {
    return taskName;
  }

  public void setTaskName(Text taskName) {
    this.taskName = taskName;
  }

  public TaskCard taskDescription(Text taskDescription) {
    this.taskDescription = taskDescription;
    return this;
  }

  /**
   * Get taskDescription
   * @return taskDescription
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getTaskDescription() {
    return taskDescription;
  }

  public void setTaskDescription(Text taskDescription) {
    this.taskDescription = taskDescription;
  }

  public TaskCard taskReport(Text taskReport) {
    this.taskReport = taskReport;
    return this;
  }

  /**
   * Get taskReport
   * @return taskReport
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getTaskReport() {
    return taskReport;
  }

  public void setTaskReport(Text taskReport) {
    this.taskReport = taskReport;
  }

  public TaskCard taskResolution(Text taskResolution) {
    this.taskResolution = taskResolution;
    return this;
  }

  /**
   * Get taskResolution
   * @return taskResolution
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getTaskResolution() {
    return taskResolution;
  }

  public void setTaskResolution(Text taskResolution) {
    this.taskResolution = taskResolution;
  }

  public TaskCard taskChanged(OffsetDateTime taskChanged) {
    this.taskChanged = taskChanged;
    return this;
  }

  /**
   * Get taskChanged
   * @return taskChanged
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getTaskChanged() {
    return taskChanged;
  }

  public void setTaskChanged(OffsetDateTime taskChanged) {
    this.taskChanged = taskChanged;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TaskCard taskCard = (TaskCard) o;
    return Objects.equals(this.taskId, taskCard.taskId) &&
        Objects.equals(this.taskType, taskCard.taskType) &&
        Objects.equals(this.taskCode, taskCard.taskCode) &&
        Objects.equals(this.taskStatus, taskCard.taskStatus) &&
        Objects.equals(this.taskProposedStatus, taskCard.taskProposedStatus) &&
        Objects.equals(this.cmtId, taskCard.cmtId) &&
        Objects.equals(this.assignedTo, taskCard.assignedTo) &&
        Objects.equals(this.managedBy, taskCard.managedBy) &&
        Objects.equals(this.parentTaskId, taskCard.parentTaskId) &&
        Objects.equals(this.taskCheckup, taskCard.taskCheckup) &&
        Objects.equals(this.taskDeadline, taskCard.taskDeadline) &&
        Objects.equals(this.taskItemNum, taskCard.taskItemNum) &&
        Objects.equals(this.taskName, taskCard.taskName) &&
        Objects.equals(this.taskDescription, taskCard.taskDescription) &&
        Objects.equals(this.taskReport, taskCard.taskReport) &&
        Objects.equals(this.taskResolution, taskCard.taskResolution) &&
        Objects.equals(this.taskChanged, taskCard.taskChanged);
  }

  @Override
  public int hashCode() {
    return Objects.hash(taskId, taskType, taskCode, taskStatus, taskProposedStatus, cmtId, assignedTo, managedBy, parentTaskId, taskCheckup, taskDeadline, taskItemNum, taskName, taskDescription, taskReport, taskResolution, taskChanged);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TaskCard {\n");
    
    sb.append("    taskId: ").append(toIndentedString(taskId)).append("\n");
    sb.append("    taskType: ").append(toIndentedString(taskType)).append("\n");
    sb.append("    taskCode: ").append(toIndentedString(taskCode)).append("\n");
    sb.append("    taskStatus: ").append(toIndentedString(taskStatus)).append("\n");
    sb.append("    taskProposedStatus: ").append(toIndentedString(taskProposedStatus)).append("\n");
    sb.append("    cmtId: ").append(toIndentedString(cmtId)).append("\n");
    sb.append("    assignedTo: ").append(toIndentedString(assignedTo)).append("\n");
    sb.append("    managedBy: ").append(toIndentedString(managedBy)).append("\n");
    sb.append("    parentTaskId: ").append(toIndentedString(parentTaskId)).append("\n");
    sb.append("    taskCheckup: ").append(toIndentedString(taskCheckup)).append("\n");
    sb.append("    taskDeadline: ").append(toIndentedString(taskDeadline)).append("\n");
    sb.append("    taskItemNum: ").append(toIndentedString(taskItemNum)).append("\n");
    sb.append("    taskName: ").append(toIndentedString(taskName)).append("\n");
    sb.append("    taskDescription: ").append(toIndentedString(taskDescription)).append("\n");
    sb.append("    taskReport: ").append(toIndentedString(taskReport)).append("\n");
    sb.append("    taskResolution: ").append(toIndentedString(taskResolution)).append("\n");
    sb.append("    taskChanged: ").append(toIndentedString(taskChanged)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

