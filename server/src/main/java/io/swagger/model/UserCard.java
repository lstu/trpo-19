package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserCard
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class UserCard   {
  @JsonProperty("user_id")
  private Integer userId = null;

  @JsonProperty("user_ad_id")
  private String userAdId = null;

  @JsonProperty("user_name")
  private String userName = null;

  @JsonProperty("user_nick")
  private String userNick = null;

  @JsonProperty("user_email")
  private String userEmail = null;

  public UserCard userId(Integer userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(example = "1", value = "")


  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public UserCard userAdId(String userAdId) {
    this.userAdId = userAdId;
    return this;
  }

  /**
   * Get userAdId
   * @return userAdId
  **/
  @ApiModelProperty(value = "")


  public String getUserAdId() {
    return userAdId;
  }

  public void setUserAdId(String userAdId) {
    this.userAdId = userAdId;
  }

  public UserCard userName(String userName) {
    this.userName = userName;
    return this;
  }

  /**
   * Get userName
   * @return userName
  **/
  @ApiModelProperty(example = "Andrew Butko", value = "")


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public UserCard userNick(String userNick) {
    this.userNick = userNick;
    return this;
  }

  /**
   * Get userNick
   * @return userNick
  **/
  @ApiModelProperty(example = "ABT", value = "")


  public String getUserNick() {
    return userNick;
  }

  public void setUserNick(String userNick) {
    this.userNick = userNick;
  }

  public UserCard userEmail(String userEmail) {
    this.userEmail = userEmail;
    return this;
  }

  /**
   * Get userEmail
   * @return userEmail
  **/
  @ApiModelProperty(value = "")


  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserCard userCard = (UserCard) o;
    return Objects.equals(this.userId, userCard.userId) &&
        Objects.equals(this.userAdId, userCard.userAdId) &&
        Objects.equals(this.userName, userCard.userName) &&
        Objects.equals(this.userNick, userCard.userNick) &&
        Objects.equals(this.userEmail, userCard.userEmail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, userAdId, userName, userNick, userEmail);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserCard {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    userAdId: ").append(toIndentedString(userAdId)).append("\n");
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("    userNick: ").append(toIndentedString(userNick)).append("\n");
    sb.append("    userEmail: ").append(toIndentedString(userEmail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

