package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.Text;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * VersionCard
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T05:35:02.507Z")

public class VersionCard   {
  @JsonProperty("docver_id")
  private Integer docverId = null;

  @JsonProperty("user_id")
  private Integer userId = null;

  @JsonProperty("doc_id")
  private Integer docId = null;

  @JsonProperty("file_id")
  private Integer fileId = null;

  @JsonProperty("docver_num")
  private String docverNum = null;

  @JsonProperty("docver_status")
  private Integer docverStatus = null;

  @JsonProperty("docver_fname")
  private String docverFname = null;

  @JsonProperty("docver_dt")
  private OffsetDateTime docverDt = null;

  @JsonProperty("docver_notes")
  private Text docverNotes = null;

  public VersionCard docverId(Integer docverId) {
    this.docverId = docverId;
    return this;
  }

  /**
   * Get docverId
   * @return docverId
  **/
  @ApiModelProperty(value = "")


  public Integer getDocverId() {
    return docverId;
  }

  public void setDocverId(Integer docverId) {
    this.docverId = docverId;
  }

  public VersionCard userId(Integer userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")


  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public VersionCard docId(Integer docId) {
    this.docId = docId;
    return this;
  }

  /**
   * Get docId
   * @return docId
  **/
  @ApiModelProperty(value = "")


  public Integer getDocId() {
    return docId;
  }

  public void setDocId(Integer docId) {
    this.docId = docId;
  }

  public VersionCard fileId(Integer fileId) {
    this.fileId = fileId;
    return this;
  }

  /**
   * Get fileId
   * @return fileId
  **/
  @ApiModelProperty(value = "")


  public Integer getFileId() {
    return fileId;
  }

  public void setFileId(Integer fileId) {
    this.fileId = fileId;
  }

  public VersionCard docverNum(String docverNum) {
    this.docverNum = docverNum;
    return this;
  }

  /**
   * Get docverNum
   * @return docverNum
  **/
  @ApiModelProperty(value = "")


  public String getDocverNum() {
    return docverNum;
  }

  public void setDocverNum(String docverNum) {
    this.docverNum = docverNum;
  }

  public VersionCard docverStatus(Integer docverStatus) {
    this.docverStatus = docverStatus;
    return this;
  }

  /**
   * Get docverStatus
   * @return docverStatus
  **/
  @ApiModelProperty(value = "")


  public Integer getDocverStatus() {
    return docverStatus;
  }

  public void setDocverStatus(Integer docverStatus) {
    this.docverStatus = docverStatus;
  }

  public VersionCard docverFname(String docverFname) {
    this.docverFname = docverFname;
    return this;
  }

  /**
   * Get docverFname
   * @return docverFname
  **/
  @ApiModelProperty(value = "")


  public String getDocverFname() {
    return docverFname;
  }

  public void setDocverFname(String docverFname) {
    this.docverFname = docverFname;
  }

  public VersionCard docverDt(OffsetDateTime docverDt) {
    this.docverDt = docverDt;
    return this;
  }

  /**
   * Get docverDt
   * @return docverDt
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getDocverDt() {
    return docverDt;
  }

  public void setDocverDt(OffsetDateTime docverDt) {
    this.docverDt = docverDt;
  }

  public VersionCard docverNotes(Text docverNotes) {
    this.docverNotes = docverNotes;
    return this;
  }

  /**
   * Get docverNotes
   * @return docverNotes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Text getDocverNotes() {
    return docverNotes;
  }

  public void setDocverNotes(Text docverNotes) {
    this.docverNotes = docverNotes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VersionCard versionCard = (VersionCard) o;
    return Objects.equals(this.docverId, versionCard.docverId) &&
        Objects.equals(this.userId, versionCard.userId) &&
        Objects.equals(this.docId, versionCard.docId) &&
        Objects.equals(this.fileId, versionCard.fileId) &&
        Objects.equals(this.docverNum, versionCard.docverNum) &&
        Objects.equals(this.docverStatus, versionCard.docverStatus) &&
        Objects.equals(this.docverFname, versionCard.docverFname) &&
        Objects.equals(this.docverDt, versionCard.docverDt) &&
        Objects.equals(this.docverNotes, versionCard.docverNotes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docverId, userId, docId, fileId, docverNum, docverStatus, docverFname, docverDt, docverNotes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VersionCard {\n");
    
    sb.append("    docverId: ").append(toIndentedString(docverId)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    docId: ").append(toIndentedString(docId)).append("\n");
    sb.append("    fileId: ").append(toIndentedString(fileId)).append("\n");
    sb.append("    docverNum: ").append(toIndentedString(docverNum)).append("\n");
    sb.append("    docverStatus: ").append(toIndentedString(docverStatus)).append("\n");
    sb.append("    docverFname: ").append(toIndentedString(docverFname)).append("\n");
    sb.append("    docverDt: ").append(toIndentedString(docverDt)).append("\n");
    sb.append("    docverNotes: ").append(toIndentedString(docverNotes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

